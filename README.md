
### Escrow Service as a Smart Contract: The Business Logic

[post](https://medium.com/coinmonks/escrow-service-as-a-smart-contract-the-business-logic-5b678ebe1955)

#### Def: Safe Remote Purchase

escrow agency as scon

#### Sec: The Business of Escrow Services

Ppl are mutual mistrusted that leads the requirement of escrow agency (第三方交易仲介) which is, say, bank, stock exchange, etc..

state0:

[ B: attempt buy X
| ℭ: Nothing
| S: attempt (sell i) X
]

transfer (S→ℭ) (2×i)

state1:

| S: attempt buy X
| ℭ: hold (2×i)
| S: attempt (sell i) X
]

transfer (B→ℭ) (2×i)

state2:

[ B: attempt buy X
| ℭ: hold (4×i)
| S: attempt (sell i) X
]

send (S→B) X
receive B X

state3:

transfer (ℭ→B) i &&& transfer (ℭ→S) (3×i)

state4:

[ B: hold X ; hold i
| ℭ: Nothing
| S: hold (3×i)
]


### Creating Smart Contracts with Smart Contract

[post](https://medium.com/coinmonks/creating-smart-contracts-with-smart-contract-d54e21d26e00)

frequent seller will need to do

S0: ℭS0 ↩ genSCon << attempt sell(i) X
B0: ℭB0 ↩ ℭS0.newPurchase B0 << attempt buy(j) X
B1: ℭB1 ↩ ℭS0.newPurchase B1 << attempt buy(k) X
